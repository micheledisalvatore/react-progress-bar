# React exercise

Make a step progress component.

![alt](https://cloud.githubusercontent.com/assets/123278/24402232/8b001ca2-13af-11e7-9028-c8e46e6f2ccb.png)

### 📖 Spec

- Number of steps is dynamic
- There's a minimum step of two and a maximum of five
- You can't jump over a step
- Write a simple test to check if the state has changed after click

### 🔥 tips

- Make use of ES6
- Use stateless components where possible
- Demonstrate the understanding of React components' life cycle
- Enhancing the component with simple animations is a nice to have

### 📦 Deliverables

- Please send the repo URL or a zip file to lev@moteefe.com
- Provide an easy setup `npm install` and `npm start`
- Tell us how much time you spent on the task

### <Scripts />:

- Install `npm install`
- Run it `npm start`
- Visit [localhost:3000](http://localhost:3000)
- Execute tests `npm test`;
- Execute tests with coverage `npm test -- --coverage`;

### 📝 Notes

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Coding time: 3~4 hours